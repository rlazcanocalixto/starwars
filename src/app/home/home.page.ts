import { Component } from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {HttpClient} from '@angular/common/http';
interface Personaje {
    name:string; 
    height: number; 
    mass: number; 
    hair_color: string; 
    skin_color: string;
    homeworld: string; 
    species: any;
}
let person: any = {};
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
constructor(private fire:AngularFirestore,private http: HttpClient) {
}
person = []
p = {name:'name', 
  height: 1 ,
  mass: 1 ,
  hair_color: "string" ,
  skin_color: "string",
  homeworld: "string" ,
  species: "any"};
searchData(){
  
  this.http.get("https://swapi.co/api/people/").subscribe((data:any)=>{
   var d = data.results;
   this.person = d;
   console.log(d);
  })
}
save(n : string, h :number, m : number,hc:string,sc:string,ho:string,sp:string ){
console.warn("enviando")
let obj = {
  name:n, 
  height: h ,
  mass: m ,
  hair_color: hc ,
  skin_color: sc,
  homeworld: ho ,
  species: sp
};

let idDoc = this.fire.createId();
this.fire.doc("/personajes/"+idDoc).set(obj);
console.log("guardado",obj)
}

ngOnInit(){
  this.searchData();
  console.log(person,"person")
}

}
