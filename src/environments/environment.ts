// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebase: {
    apiKey: "AIzaSyAO399nUcM8M8DnmgoBsGXrnPI3Ry6f6d8",
    authDomain: "starwars-df5c4.firebaseapp.com",
    databaseURL: "https://starwars-df5c4.firebaseio.com",
    projectId: "starwars-df5c4",
    storageBucket: "starwars-df5c4.appspot.com",
    messagingSenderId: "276993665729",
    appId: "1:276993665729:web:58a08a4388815af8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
